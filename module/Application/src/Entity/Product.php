<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * This class represents a registered Product.
 * @ORM\Entity()
 * @ORM\Table(name="Product")
 */
class Product 
{

    
    /**
     * @ORM\Id
     * @ORM\Column(name="id")
     * 
     */
    protected $id;

    /** 
     * @ORM\Column(name="title")  
     */
    protected $title;
    
    /** 
     * @ORM\Column(name="body_html")  
     */
    protected $body_html;

    /** 
     * @ORM\Column(name="vendor")  
     */
    protected $vendor;
    
   /** 
     * @ORM\Column(name="product_type")  
     */
    protected $product_type;
       /** 
     * @ORM\Column(name="created_at")  
     */
    protected $created_at;
         /** 
     * @ORM\Column(name="handle")  
     */
    protected $handle;
           /** 
     * @ORM\Column(name="updated_at")  
     */
    protected $updated_at;
           /** 
     * @ORM\Column(name="published_at")  
     */
    protected $published_at;
           /** 
     * @ORM\Column(name="template_suffix")  
     */
    protected $template_suffix;
           /** 
     * @ORM\Column(name="tags")  
     */
    protected $tags;
      /** 
     * @ORM\Column(name="collection_id")  
     */
    protected $collection_id;
    /**
     * Returns product ID.
     * @return integer
     */
    public function getId() 
    {
        return $this->id;
    }

    /**
     * Sets product ID. 
     * @param int $id    
     */
    public function setId($id) 
    {
        $this->id = $id;
    }

    /**
     * Returns title.     
     * @return string
     */
    public function getTitle() 
    {
        return $this->title;
    }

    /**
     * Sets title.     
     * @param string $title
     */
    public function setTitle($title) 
    {
        $this->title = $title;
    }
    
    /**
     * Returns full name.
     * @return string     
     */
    public function getBody_html() 
    {
        return $this->content;
    }       

    /**
     * Sets full name.
     * @param string $body_html
     */
    public function setBody_html($body_html) 
    {
        $this->body_html = $body_html;
    }
   /**
     * Returns status.
     *      
     */
    public function getHandle() 
    {
        return $this->handle;
    }
   /**
     * Sets status.
     *    
     */
    public function setHandle($handle) 
    {
        $this->handle = $handle;
    }   
    /**
     * Returns status.
     * @return int     
     */
    public function getVendor() 
    {
        return $this->vendor;
    }

  
    /**
     * 
     * @return this    
     */
    public function setVendo($vendor) 
    {
        $this->vendor = $vendor;
        
    }   
    
    /**
     * Returns password.
     * @return datetime
     */
    public function getCreated_at() 
    {
       return $this->created_at; 
    }
    
    /**
     * Sets password.     
     * @param datetime $date
     */
    public function setCreated_at($created_at) 
    {
        $this->created_at = $created_at;
    }
       
    /**
     * Sets tags.
     * @param sting $Tags     
     */
    public function setTags($tags) 
    {
        $this->tags = $tags;
    }  
        /**
     * Returns tags.
     * @return string     
     */
    public function getTags() 
    {
        return $this->tags;
    }
     /**
     * Sets tags.
     * @param double $     
     */
    public function setCollection_id($collection_id) 
    {
        $this->collection_id = $collection_id;
    }  
        /**
     * Returns tags.
     * @return string     
     */
    public function getCollection_id() 
    {
        return $this->collection_id;
    }

  
}
