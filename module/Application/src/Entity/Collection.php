<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * This class represents a registered Collection.
 * @ORM\Entity()
 * @ORM\Table(name="Collection")
 */
class Collection 
{

    
    /**
     * @ORM\Id
     * @ORM\Column(name="id")
     * 
     */
    protected $id;

    /** 
     * @ORM\Column(name="title")  
     */
    protected $title;
    
    /** 
     * @ORM\Column(name="body_html")  
     */
    protected $body_html;

    

         /** 
     * @ORM\Column(name="handle")  
     */
    protected $handle;

    /**
     * Returns collection ID.
     * @return integer
     */
    public function getId() 
    {
        return $this->id;
    }

    /**
     * Sets collection ID. 
     * @param int $id    
     */
    public function setId($id) 
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Returns title.     
     * @return string
     */
    public function getTitle() 
    {
        return $this->title;
    }

    /**
     * Sets title.     
     * @param string $title
     */
    public function setTitle($title) 
    {
        $this->title = $title;
    }
    
    /**
     * Returns body html.
     * @return string     
     */
    public function getBody_html() 
    {
        return $this->content;
    }       

    /**
     * Sets body html.
     * @param string $body_html
     */
    public function setBody_html($body_html) 
    {
        $this->body_html = $body_html;
    }
   /**
     * Returns handle.
     *      
     */
    public function getHandle() 
    {
        return $this->author;
    }
   /**
     * Sets handle.
     *    
     */
    public function setHandle($handle) 
    {
        $this->handle = $handle;
    } 
}