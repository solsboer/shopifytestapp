<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Application\Controller\Factory;

use Application\Controller\IndexController;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;

/**
 * Description of IndexControolerFactory
 *
 * @author sergiy
 */
class IndexControllerFactory implements FactoryInterface {
    //put your code here
 public function __invoke(ContainerInterface $container, $requestedName, array $options = null) {
    $config = $container->get("Config");
    $shopifyclient = new \ZfrShopify\ShopifyClient($config['zfr_shopify']);
    $entityManager = $container->get('doctrine.entitymanager.orm_default');
    return new IndexController($shopifyclient,$entityManager);
    }

}

