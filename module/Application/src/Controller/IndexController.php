<?php

/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Entity\Collection;
use Application\Entity\Product;

class IndexController extends AbstractActionController {

    protected $collections;
    protected $products;
    protected $shopifyClient;

    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    public function __construct($shopifyclient, $entityManager) {
        $this->shopifyClient = $shopifyclient;
        $this->entityManager = $entityManager;
        $this->collections = $this->entityManager->getRepository(Collection::class)
                ->findAll();
    }

    public function indexAction() {
        // $this->products = $this->entityManager->getRepository(Product::class)
        //  ->findAll();



        return new ViewModel([
            'collections' => $this->collections,
            'products' => $this->entityManager->getRepository(Product::class)
                    ->findAll()
        ]);
    }

    public function updateAction() {
        foreach ($this->shopifyClient->getCustomCollections(['fields' => 'id']) as $collection) {
            var_dump($collection['id']);
        }

        foreach ($this->shopifyClient->getCustomCollections(['fields' => 'id']) as $collection) {
            foreach ($this->shopifyClient->getProductsIterator(['collection_id' => $collection['id']]) as $products) {
                if ($this->entityManager->getRepository(Product::class)
                                ->findOneBy(['id' => $products['id']]) == null) {
                    $product = new Product();
                    $product->setId($products['id']);
                    $product->setTitle($products['title']);
                    $product->setBody_html($products['body_hmtl']);
                    $product->setHandle($products['handle']);
                    $product->setCollection_id($collection['id']);
                    $this->entityManager->persist($product);
                    $this->entityManager->flush();
                } else {
                    $product = $this->entityManager->getRepository(Product::class)
                            ->findOneBy(['id' => $products['id']]);
                    $product->setCollection_id($product->getCollection_id() . ',' . $collection['id']);
                }
            }
        }

        foreach ($this->shopifyClient->getCustomCollections() as $collections) {
            if ($testForCollection = $this->entityManager->getRepository(Collection::class)
                            ->findOneBy(['id' => $collections['id']]) == null) {
                $collection = new Collection();
                $collection->setId($collections['id']);
                $collection->setTitle($collections['title']);
                $collection->setBody_html($collections['body_hmtl']);
                $collection->setHandle($collections['handle']);
                $this->entityManager->persist($collection);
                $this->entityManager->flush();
            }
        }
        return $this->redirect()->toRoute('application', ['action' => 'index']);
    }

    public function showAction() {
        $id = (int) $this->params()->fromRoute('id', -1);
        if ($id < 1) {
            $this->getResponse()->setStatusCode(404);
            return;
        }
        $products_array = array();
        $products = $this->entityManager->getRepository(Product::class)
                ->findAll();
        foreach ($products as $product) {
            $ids = explode(",", $product->getCollection_id());
            if (in_array($id, $ids))
                array_push($products_array, $product);
        }

        return new ViewModel([
            'collections' => $this->collections,
            'products' => $products_array,
        ]);
    }

}
