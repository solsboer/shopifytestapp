<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */

return [
    'zfr_shopify' => [
        'private_app'   => true,
        'api_key'       => 'ce2340216b08bf5f2ed549574a2cfffd', // In public app, this is the app ID
	'password'    => '8e8630eab75021e39d1a4e0ab4dfbd66',      
        'shop'          => 'sintel.myshopify.com',
    ],
       'doctrine' => [        
        // migrations configuration
        'migrations_configuration' => [
            'orm_default' => [
                'directory' => 'data/Migrations',
                'name'      => 'Doctrine Database Migrations',
                'namespace' => 'Migrations',
                'table'     => 'migrations',
            ],
        ],
    ],
];
